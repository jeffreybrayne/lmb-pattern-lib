LMB JavaScript Coding Conventions

```javascript
var foo = "bar" // not var foo = 'bar';
var obj = {}; // not var obj = new Object();

```
